'use strict';

/**
 * @ngdoc overview
 * @name gingr-admin
 * @description
 * # gingr-admin
 *
 * Main module of the application.
 */
angular
  .module('gingr-admin', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch','satellizer','ngMaterial','smart-table','ui.bootstrap'
  ])
  .config(function ($routeProvider,$authProvider) {

    $authProvider.loginUrl = "http://api.gingr.es/login";
    $authProvider.signupUrl = "http://api.gingr.es/signup";
    $authProvider.tokenName = "token";
    $authProvider.tokenPrefix = "myApp";
    $authProvider.tokenRoot = "result";



    $routeProvider


      .when('/users', {
        templateUrl: 'views/users.html',
        controller: 'UsersCtrl',
        controllerAs: 'users',
        data:{requiredLogin:true}
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/', {
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl',
        controllerAs: 'dashboard',
        data:{requiredLogin:true}
      })
      .otherwise({
        redirectTo: '/login'
      });
  }).run(function($rootScope,$http,$auth,$location,$mdToast){


    $rootScope.$on( "$routeChangeStart", function(event, next, current) {

      var requiredLogin = false;
      // check if this state need login
      if (next.data && next.data.requiredLogin)
        requiredLogin = true;

      // if yes and if this user is not logged in, redirect him to login page
      if (requiredLogin && !$auth.isAuthenticated()) {
        event.preventDefault();
        $location.path( "/login" );
        $mdToast.show(
          $mdToast.simple()
            .textContent('You have to be logged !')
            .position("right" )
            .hideDelay(3000)
        );
      }

    });

});


angular.module('gingr-admin')
  .controller('LoginCtrl', function ($auth,$location) {

    var vm = this;
    this.login = function(){
      $auth.login({
          email: vm.email,
          password: vm.password
        })
        .then(function(response){
          // Si se ha logueado correctamente, lo tratamos aquí.
          // Podemos también redirigirle a una ruta
          //$location.path("/private")
          console.log(response);
          $location.path("/");


        })
        .catch(function(response){
          // Si ha habido errores llegamos a esta parte
        });
    }


  });

angular.module('gingr-admin')
  .controller('LogoutCtrl', function ($auth,$location) {
    var vm=this;
    this.logout=function()
    {
      $auth.logout()
        .then(function() {
          // Desconectamos al usuario y lo redirijimos
          $location.path("/login");
        });
    }

  });


angular.module('gingr-admin')
  .controller('MenuCtrl', function ($auth,$location,$rootScope) {
    var vm=this;
    this.Auth=$auth;
    this.isAuthenticated=$auth.isAuthenticated();
    $rootScope.path=$location.path();


    $rootScope.$on('$routeChangeSuccess', function (e, current, pre) {
      var fullRoute = current.$$route.originalPath,
        routeParams = current.params,
        resolvedRoute;

      console.log(fullRoute);
      $rootScope.path=fullRoute;
    });


  });
