'use strict';

/**
 * @ngdoc function
 * @name gingr-admin.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the gingr-admin
 */
angular.module('gingr-admin')
  .controller('DashboardCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
