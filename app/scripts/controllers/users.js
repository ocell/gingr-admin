'use strict';

/**
 * @ngdoc function
 * @name gingr-admin.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the gingr-admin
 */
angular.module('gingr-admin')
  .controller('UsersCtrl', function ($scope,$http,API_URL,MEDIA_URL,$mdToast,$mdDialog) {

    $scope.users = null;
    $scope.usersO = null;
    $scope.mediaurl=MEDIA_URL;

    $scope.itemsByPage=50;
    $scope.itemsDisplayed=10;
    $scope.editor=false;
    //if tou want something to dont be shown on basic user details, add here
    var hideFromBasic = ["media","location","cover_image","avatar_image","review","profile","services","speaks","travel","availability","identify","verified"];
    $scope.userInit=function(){


      $http.post(API_URL+"/admin/listUser").then(function (response) {
        $scope.usersO = response.data.result;
        $scope.users = angular.copy(response.data.result);

        $scope.itemsDisplayed = Math.round(response.data.result.length/50);

        var tableElement = document.getElementById('tibli');

        var stickyTable = lrStickyHeader(tableElement);

        var parentElement = document.getElementById('scrollPanel');
        var stickyTable2 = lrStickyHeader(tableElement, {parent: parentElement});


      }, function (response) {
        console.log(' error with status : ' + response.status);
      }).catch(function () {
        console.log('Contact error with status ');
      });
    };
    $scope.userInit();

    $scope.activateUser=function(user){
      $http.post(API_URL+"/admin/active",{username:user.username,active:user.active}).then(function (response) {
        $scope.messageHandler(response);
      }, function (response) {
        $scope.showToast("Error");
      }).catch(function () {
        console.log('Contact error with status ');
      });
    };

    $scope.resendEmail=function(user){
      $http.post(API_URL+"/admin/sendEmail ",{username:user.username}).then(function (response) {
        $scope.messageHandler(response);
      }, function (response) {
        $scope.showToast("error");
      }).catch(function () {
        console.log('Contact error with status ');
      });
    };
    $scope.resendSMS=function(user){
      $http.post(API_URL+"/admin/sendSms",{username:user.username}).then(function (response) {
        $scope.messageHandler(response);
      }, function (response) {
        $scope.showToast("error");
      }).catch(function () {
        console.log('Contact error with status ');
      });
    };

    $scope.verifyUser=function(user){
      $http.post(API_URL+"/admin/verification ",{username:user.username}).then(function (response) {
        $scope.messageHandler(response);
        $scope.userOriginal.verified=1;//TODO
        $scope.user.verified=1;//TODO
      }, function (response) {
        $scope.showToast("error");
      }).catch(function () {
        console.log('Contact error with status ');
      });
    };
    $scope.media={};
    $scope.approveMedia=function(media, approve){
      $scope.media=media;
      $http.post(API_URL+"/admin/verifyImage ",{username:$scope.user.username,filename:media.filename,state:approve}).then(function (response) {

        $scope.messageHandler(response);
        if(response.status==200){
          //$scope.approve=response;
          $scope.media.approved=response.data.result.state;
        }
      }, function (response) {
        $scope.showToast("error");
      }).catch(function () {
        console.log('Contact error with status ');
      });
    };
    $scope.saveUser=function(){
      $http.post(API_URL+"/admin/updateAccount ",$scope.user).then(function (response) {
        $scope.messageHandler(response,$scope.userInit);
        if(response.data.status==200)
          $scope.editor=false;

      }, function (response) {
        $scope.showToast("error");
      }).catch(function () {
        console.log('Contact error with status ');
      });
    };


    $scope.editUser=function(username){
      $http.post(API_URL+"/admin/selectFullUser",{"username":username}).then(function (response) {
        console.log(response.data.result);
        $scope.user = response.data.result.user;
        $scope.userOriginal = angular.copy($scope.user);
        $scope.editor=true;

      }, function (response) {
        console.log(' error with status : ' + response.status);
      }).catch(function () {
        console.log('Contact error with status ');
      });
    };

    $scope.showToast=function(message){
      if(message==undefined){message="operation success"}
      $mdToast.show(
        $mdToast.simple()
          .textContent(message)
          .position("right" )
          .hideDelay(3000)
      );
    };

    $scope.basicShow=function(key){
      if (hideFromBasic.indexOf(key)==-1){
        return true;
      }else{
        return false;
      }
    };
    $scope.messageHandler=function(response,func){
      if(response.data.status==200){
        $scope.showToast();
        if(func)
          func();

      }else{
        $scope.showToast(response.data.exception);
      }
    };

    $scope.openCensoreDialog=function(ev,media){
      $mdDialog.show({
          //controller: DialogController,
          templateUrl: 'views/censore.html',
          parent: angular.element(document.body),
          targetEvent: ev,
          clickOutsideToClose:true,
          fullscreen: true // Only for -xs, -sm breakpoints.
        })
        .then(function(answer) {
          $scope.status = 'You said the information was "' + answer + '".';
        }, function() {
          $scope.status = 'You cancelled the dialog.';
        });
    }



  });
$(function() {

});

