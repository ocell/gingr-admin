'use strict';

/**
 * @ngdoc function
 * @name gingr-admin.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the gingr-admin
 */
angular.module('gingr-admin')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
